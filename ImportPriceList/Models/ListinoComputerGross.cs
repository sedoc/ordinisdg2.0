﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace ImportPriceList.EF
{
    public partial class ListinoComputerGross
    {
        public OrdiniSDG currentContext;

        public static List<object> getList(int rowNum = 100)
        {
            return new OrdiniSDG().ListinoComputerGross.OrderBy(c => c.Codice).Take(rowNum).ToList<object>();
        }

        public static ListinoComputerGross getItem(string id)
        {
            return new OrdiniSDG().ListinoComputerGross.Where(i => i.Codice == id).FirstOrDefault();
        }

        public static List<object> searchItem(string filter, int rowNum = 100)
        {
            if (!string.IsNullOrEmpty(filter))
                return new OrdiniSDG().ListinoComputerGross.Where(i => i.Codice.Contains(filter) || i.Descrizione.Contains(filter)).Take(rowNum).ToList<object>();
            else
                return getList();
        }

        /*********************** CRUD *****************************************/
        public  static ListinoComputerGross Save(ListinoComputerGross model)
        {
            try
            {
                var context = new OrdiniSDG();

                context.ListinoComputerGross.Add(model);
                context.SaveChanges();

                return model;
            }
            catch (Exception ecc)
            {
                return null;
            }
        }

        public static ListinoComputerGross Update(ListinoComputerGross model)
        {
            try
            {
                var context = new OrdiniSDG();

                var newModel = context.ListinoComputerGross.Where(x => x.Codice == model.Codice).FirstOrDefault();

                newModel.Prezzo_Listino = model.Prezzo_Listino;
                newModel.Sconto = model.Sconto;
                newModel.Prezzo_Rivenditore = model.Prezzo_Rivenditore;

                context.SaveChanges();

                return newModel;
            }
            catch (Exception ecc)
            {
                return null;
            }
        }

        public static int DeleteAll()
        {
            try
            {
                var context = new OrdiniSDG();

                return context.Database.ExecuteSqlCommand("TRUNCATE TABLE ListinoComputerGross");
            }
            catch (Exception ecc)
            {
                return 0;
            }
        }
    }
}