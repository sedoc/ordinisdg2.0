﻿using CustomLog;
using ImportPriceList.EF;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportPriceList
{
    class Program
    {
        static void Main(string[] args)
        {
            string registryFile = ConfigurationManager.AppSettings["PathPriceList"] + ConfigurationManager.AppSettings["FileRegistry"];
            string priceFile = ConfigurationManager.AppSettings["PathPriceList"] + ConfigurationManager.AppSettings["FilePrice"];

            Logger logger = new Logger();
            logger._logFolder = new FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).DirectoryName + "\\log";

            if (!Directory.Exists(logger._logFolder))
                Directory.CreateDirectory(logger._logFolder);


            if(!File.Exists(registryFile) || !File.Exists(priceFile))
            {
                logger.log(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff") + " -- Nessun file trovato");
                return;
            }


            logger.log(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff") + " -- Start import");

            int row = ListinoComputerGross.DeleteAll();

            logger.log(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff") + " -- Eliminate " + row.ToString() + " righe");

            var lines = File.ReadLines(registryFile);
            int index = 0;
            foreach (var line in lines)
            {
                if (index < Int32.Parse(ConfigurationManager.AppSettings["RowToSkip"]))
                {
                    index++;
                    continue;
                }
                String[] element = line.Split(';');

                ListinoComputerGross model = new ListinoComputerGross();
                model.Codice = element[1].Trim();
                model.Descrizione = element[4].Trim();
                model.Prezzo_Listino = 0;
                model.Sconto = 0;
                model.Prezzo_Rivenditore = 0;
                model.Disp_Immediata = "";
                model.Disp_Futura = "";

                model.Vendor = element[5].Trim();
                model.Supercategoria = element[7].Trim();
                model.Livello1 = element[7].Trim();
                model.Livello2 = element[9].Trim();
                model.Livello3 = element[11].Trim();

                ListinoComputerGross.Save(model);

                index++;
           }

            logger.log(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff") + " -- Import ListiniG finito con  " + index + " righe");


            lines = File.ReadLines(priceFile);
            index = 0;
            foreach (var line in lines)
            {
                if (index < Int32.Parse(ConfigurationManager.AppSettings["RowToSkip"]))
                {
                    index++;
                    continue;
                }
                String[] element = line.Split(';');

                ListinoComputerGross model = ListinoComputerGross.getItem(element[0].Trim());

                if(model == null)
                {
                    logger.log(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff") + " -- Prezzo per " + element[0].Trim() + " non aggiornato ");
                    continue;
                }

                model.Prezzo_Listino = Convert.ToDecimal(element[5].Trim());
                model.Sconto = 0;
                model.Prezzo_Rivenditore = Convert.ToDecimal(element[7].Trim());

                ListinoComputerGross.Update(model);

                index++;
            }

            logger.log(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff") + " -- Import ListiniG finito con  " + index + " righe");

        }
    }
}
