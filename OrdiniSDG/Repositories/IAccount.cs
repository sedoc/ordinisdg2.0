﻿using OrdiniSDG.Classes.EF;
using OrdiniSDG.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrdiniSDG.Repositories
{
    public interface IAccount
    {
        List<USR_UserViewModel> getUsers();

        USR_User getUserFromUsername(string username);
        USR_User addUser(USR_User user);
        List<RLE_RoleViewModel> getRoles();
        USR_User updateUser(USR_User user);
        bool isExistUserName(string userName);
    }
}
