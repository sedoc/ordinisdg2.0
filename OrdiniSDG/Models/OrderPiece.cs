﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrdiniSDG.Models
{
    public class OrderPiece
    {
        public DateTime? CreationDate { get; set; }
        public int? SAP_ORD_ID { get; set; }
        public string CustomerDescription { get; set; }
        public decimal? TotalUnitCost { get; set; }
        public decimal? Gain { get; set; }
        public string AgentName { get; set; }
        public decimal? DetailTotalAmount { get; set; }
    }
}