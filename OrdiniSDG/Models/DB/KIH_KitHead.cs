﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OrdiniSDG.Classes;

namespace OrdiniSDG.Classes.EF
{
    public partial class KIH_KitHead
    {
        public OrdiniSDGEntities currentContext;
        
        public static List<object> getKitHead(int userID)
        {
            return Globals.getEFContext().KIH_KitHead.Where(k =>k.CreationUser == userID && k.AnnullationDate ==null).OrderBy(k=>k.KIH_ID).ToList<object>();
        }

        public static object create(KIH_KitHead kitH, int userid)
        {
            OrdiniSDGEntities context = Globals.getEFContext();

            try
            {
                kitH.CreationDate = DateTime.Now;
                kitH.CreationUser = userid;

                context.KIH_KitHead.Add(kitH);

                context.SaveChanges();
            }
            catch (Exception ex)
            {

            }
            return kitH;
        }

        public static object update(KIH_KitHead kitH, int userid)
        {
            OrdiniSDGEntities context = Globals.getEFContext();
            //Prendo l'oggetto dal db per averlo con un context
            kitH.KID_KitDetail = null;
            KIH_KitHead KIT = context.KIH_KitHead.Where(k => k.KIH_ID == kitH.KIH_ID).FirstOrDefault();
            Utility.CopyProperties(kitH, KIT);
            try
            {
                
                context.SaveChanges();
            }
            catch (Exception ex)
            {

            }
            return KIT;
        }

        public static object delete(KIH_KitHead kitH, int userid)
        {
            OrdiniSDGEntities context = Globals.getEFContext();

            //Prendo l'oggetto dal db per averlo con un context
            KIH_KitHead KIT = context.KIH_KitHead.Where(k => k.KIH_ID == kitH.KIH_ID).FirstOrDefault();
            
            try
            {
                KIT.AnnullationDate = DateTime.Now;
                KIT.AnnullationUser = userid;

                context.SaveChanges();
            }
            catch (Exception ex)
            {

            }
            return KIT;
        }

        
    }
}