﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OrdiniSDG.Classes;

namespace OrdiniSDG.Classes.EF
{
    public partial class vw_KitHead
    {
        public OrdiniSDGEntities currentContext;

        public static List<object> getKitHead(int userID)
        {
            return Globals.getEFContext().vw_KitHead.Where(k =>k.CreationUser == userID && k.AnnullationDate ==null).OrderBy(k=>k.KIH_ID).ToList<object>();
        }
        
        
    }
}