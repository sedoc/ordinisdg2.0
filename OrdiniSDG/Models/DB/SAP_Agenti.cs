﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OrdiniSDG.Classes;

namespace OrdiniSDG.Classes.EF
{
    public partial class SAP_Agenti
    {
        private static List<object> convertToModel(List<SAP_Agenti> list)
        {
            return list.Select(l => new {
                AgentCode=l.SlpCode,
                AgentDesc=l.SlpName
            }).ToList<object>();
        }

        public static List<object> getList(string filter)
        {
            if (!string.IsNullOrEmpty(filter))
                return convertToModel(Globals.getEFContext().SAP_Agenti.Where(a => a.SlpName.Contains(filter)).OrderBy(c => c.SlpCode).ToList());
            else
                return convertToModel(Globals.getEFContext().SAP_Agenti.Take(30).OrderBy(a => a.SlpCode).ToList());
        }

        public static string getAgentFromCode(string code)
        {
            return Globals.getEFContext().SAP_Agenti.Where(a => a.SlpCode == code).Select(x => x.SlpName).FirstOrDefault();
        }
    }
}