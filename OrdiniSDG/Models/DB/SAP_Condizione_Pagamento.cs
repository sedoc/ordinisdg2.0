﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OrdiniSDG.Classes;

namespace OrdiniSDG.Classes.EF
{
    public partial class SAP_Condizione_Pagamento
    {
        private static List<object> convertToModel(List<SAP_Condizione_Pagamento> list)
        {
            return list.Select(l => new {
                PayCondCode= l.GroupNum,
                PayCondDesc=l.PymntGroup
            }).ToList<object>();
        }

        public static List<object> getList(string filter)
        {
            if (filter==null)
                return convertToModel(Globals.getEFContext().SAP_Condizione_Pagamento.OrderBy(c => c.PymntGroup).ToList());
            else
                return convertToModel(Globals.getEFContext().SAP_Condizione_Pagamento.Where(c=>c.PymntGroup.Contains(filter)).OrderBy(c => c.PymntGroup).ToList());
        }
    }
}