﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OrdiniSDG.Classes;

namespace OrdiniSDG.Classes.EF
{
    public partial class SAP_Clienti
    {
        public OrdiniSDGEntities currentContext;

        private static List<object> convertToModel(List<SAP_Clienti> list)
        {
            return list.Select(l => new {
                CustomerCode=l.CardCode,
                CustomerDescription= l.CardName,
                PayMethCode=l.PymCode,
                PayCondCode=l.GroupNum
            }).ToList<object>();
        }
        
        public static List<object> getList(string filter)
        {
            if(!string.IsNullOrEmpty(filter))
                return convertToModel(Globals.getEFContext().SAP_Clienti.Where(c=>(c.CardCode+c.CardName).Contains(filter)).OrderBy(c => c.CardName).ToList());
            else
                return convertToModel(Globals.getEFContext().SAP_Clienti.Take(10).OrderBy(c => c.CardName).ToList());
        }
    }
}