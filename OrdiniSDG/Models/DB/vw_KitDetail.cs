﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OrdiniSDG.Classes;

namespace OrdiniSDG.Classes.EF
{
    public partial class vw_KitDetail
    {
        public OrdiniSDGEntities currentContext;

        public static List<object> getKitDetailList(int kitHeadID)
        {
            return Globals.getEFContext().vw_KitDetail.Where(d => d.KitHeadCode == kitHeadID && d.AnnullationDate == null).OrderBy(d => d.KID_ID).ToList<object>();
        }


    }
}