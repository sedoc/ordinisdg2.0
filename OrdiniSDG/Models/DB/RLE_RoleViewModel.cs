﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrdiniSDG.Models.DB
{
    public class RLE_RoleViewModel
    {
        public int RLE_ROLE1 { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> CreationDate { get; set; }
        public Nullable<int> CreationUser { get; set; }
        public Nullable<System.DateTime> DelationDate { get; set; }
        public Nullable<int> DelationUser { get; set; }
    }
}