﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OrdiniSDG.Classes;

namespace OrdiniSDG.Classes.EF
{
    public partial class OPR_KID_ODetail_KDetail
    {
        public static void create(int OrderDetailId,int OrderHeadId,int KitHeadId)
        {
            OrdiniSDGEntities context = Globals.getEFContext();
            var kidList = context.vw_KitDetail.Where(k => k.KitHeadCode == KitHeadId).ToList();

            try
            {
                foreach (vw_KitDetail kid in kidList)
                {
                    OPR_KID_ODetail_KDetail toAdd = new OPR_KID_ODetail_KDetail();
                    toAdd.KID_ID = kid.KID_ID;
                    toAdd.OPR_ID = OrderDetailId;
                    toAdd.OrderHeadCode = OrderHeadId;
                    toAdd.KitHeadCode = KitHeadId;
                    toAdd.ArtCode = kid.ArtCode;
                    toAdd.ArtDescr = kid.ArtDescr; // prende descrizione Articolo dal context
                    toAdd.Supplier = kid.Supplier;
                    toAdd.Quantity = (int)kid.Quantity;
                    toAdd.UnitCost = (decimal)kid.UnitCost;
                    toAdd.kitDetailCost = kid.kitDetailCost;
                    toAdd.Producer = kid.Producer;
                    toAdd.AreaCode = kid.AreaCode;
                    toAdd.ClassCode = kid.ClassCode;
                    toAdd.SectorCode = kid.SectorCode;
                    toAdd.CreationDate = DateTime.Now;

                    context.OPR_KID_ODetail_KDetail.Add(toAdd);
                }

                context.SaveChanges();
            }
            catch(Exception ex)
            {
                throw ex;
            }

        }

    }
}