﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OrdiniSDG.Classes;

namespace OrdiniSDG.Classes.EF
{
    public partial class SAP_Articoli
    {
        public OrdiniSDGEntities currentContext;

        public static List<object> getList(int rowNum=100)
        {
            return Globals.getEFContext().SAP_Articoli.OrderBy(c => c.ItemName).Take(rowNum).ToList<object>();
        }

        public static object getItem(string id)
        {
            return Globals.getEFContext().SAP_Articoli.Where(i => i.ItemCode == id).FirstOrDefault();
        }

        public static List<object> searchItem(string filter, int rowNum = 100)
        {
            if (!string.IsNullOrEmpty(filter))
                return Globals.getEFContext().SAP_Articoli.Where(i => i.ItemCode.Contains(filter) || i.ItemName.Contains(filter) || i.U_SED_CodEx.Contains(filter)).Take(rowNum).ToList<object>();
            else
                return getList();
        }
    }
}