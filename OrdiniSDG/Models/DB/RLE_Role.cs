﻿using OrdiniSDG.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrdiniSDG.Classes.EF
{
    public partial class RLE_Role
    {
        public OrdiniSDGEntities currentContext;

        public static List<RLE_Role> getList()
        {
            return Globals.getEFContext().RLE_Role.Where(r=>r.Description!=string.Empty).ToList();
        }

        
    }
}