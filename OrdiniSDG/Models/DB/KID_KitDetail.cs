﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OrdiniSDG.Classes;

namespace OrdiniSDG.Classes.EF
{
    public partial class KID_KitDetail
    {
        public OrdiniSDGEntities currentContext;

        public static List<object> getKitDetailList(int kitHeadID)
        {
            return Globals.getEFContext().KID_KitDetail.Where(d => d.KitHeadCode == kitHeadID && d.AnnullationDate ==null).OrderBy(d => d.KID_ID).ToList<object>();
        }

        public static object create(KID_KitDetail kitD, int userid)
        {
            OrdiniSDGEntities context = Globals.getEFContext();

            try
            {
                kitD.CreationDate = DateTime.Now;
                kitD.CreationUser = userid;
                context.KID_KitDetail.Add(kitD);

                context.SaveChanges();

                if (OPR_OrderDetail.updateCosts(kitD.KitHeadCode) == false)
                    Console.WriteLine("false");
                else
                    Console.WriteLine("true");
            }
            catch (Exception ex)
            {

            }
            return kitD;
        }

        public static object update(KID_KitDetail kitD, int userid)
        {
            OrdiniSDGEntities context = Globals.getEFContext();
            //Prendo l'oggetto dal db per averlo con un context
            kitD.KIH_KitHead = null;
            KID_KitDetail KID = context.KID_KitDetail.Where(k => k.KID_ID == kitD.KID_ID).FirstOrDefault();
            Utility.CopyProperties(kitD, KID);
            try
            {
                context.SaveChanges();

                if (OPR_OrderDetail.updateCosts(kitD.KitHeadCode) == false)
                    Console.WriteLine("false");
                else
                    Console.WriteLine("true");
            }
            catch (Exception ex)
            {

            }
            return KID;
        }

        public static object delete(KID_KitDetail kitD, int userid)
        {
            OrdiniSDGEntities context = Globals.getEFContext(); 

            //Prendo l'oggetto dal db per averlo con un context
            KID_KitDetail KID = context.KID_KitDetail.Where(k => k.KID_ID == kitD.KID_ID).FirstOrDefault();
            try
            {
                KID.AnnulationUser = userid;
                KID.AnnullationDate = DateTime.Now;

                context.SaveChanges();

                if (OPR_OrderDetail.updateCosts(kitD.KitHeadCode) == false)
                    Console.WriteLine("false");
                else
                    Console.WriteLine("true");


            }
            catch (Exception ex)
            {

            }
            return KID;
        }

        
        
    }
}