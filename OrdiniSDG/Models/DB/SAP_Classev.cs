﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OrdiniSDG.Classes;

namespace OrdiniSDG.Classes.EF
{
    public partial class SAP_CLASSEV
    {
        private static List<object> convertToModel(List<SAP_CLASSEV> list)
        {
            return list.Select(l => new {
                ClassCode=l.U_COD_CLASSE_V,
                ClassDesc = l.U_DESC_CLASSE
            }).ToList<object>();
        }

        public static List<object> getList(string filter)
        {
            if (!string.IsNullOrEmpty(filter))
                return convertToModel(Globals.getEFContext().SAP_CLASSEV.Where(a => a.U_COD_PADRE==filter).OrderBy(c => c.U_COD_CLASSE_V).ToList());
            else
                return convertToModel(Globals.getEFContext().SAP_CLASSEV.OrderBy(a => a.U_COD_CLASSE_V).ToList());
        }
    }
}