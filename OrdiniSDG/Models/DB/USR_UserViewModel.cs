﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrdiniSDG.Models.DB
{
    public class USR_UserViewModel
    {
        public int USR_ID { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int Role { get; set; }
        public string RoleName { get; set; }
        public System.DateTime CreationDate { get; set; }
        public Nullable<int> UserModify { get; set; }
        public Nullable<System.DateTime> DeletingDate { get; set; }
        public Nullable<System.DateTime> LastLoginDate { get; set; }
        public Nullable<System.DateTime> LastPasswordChangedDate { get; set; }
        public string GuidRestorePass { get; set; }
        public Nullable<System.DateTime> VerifDateGuidRP { get; set; }
        public string USR_ID_SAP { get; set; }
    }
}