﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OrdiniSDG.Classes;

namespace OrdiniSDG.Classes.EF
{
    public partial class SAP_AREA_AFFARI
    {
        private static List<object> convertToModel(List<SAP_AREA_AFFARI> list)
        {
            return list.Select(l => new {
                AreaCode=l.U_COD_AREA_AFF,
                AreaDesc = l.U_DESC_AREA_AFF
            }).ToList<object>();
        }

        public static List<object> getList(string filter)
        {
            return convertToModel(Globals.getEFContext().SAP_AREA_AFFARI.OrderBy(a => a.U_COD_AREA_AFF).ToList());
        }
    }
}