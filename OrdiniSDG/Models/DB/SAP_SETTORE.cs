﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OrdiniSDG.Classes;

namespace OrdiniSDG.Classes.EF
{
    public partial class SAP_SETTORE
    {
        private static List<object> convertToModel(List<SAP_SETTORE> list)
        {
            return list.Select(l => new {
                SectorCode=l.U_COD_SETTORE,
                SectorDesc = l.U_DESCR_SETTORE
            }).ToList<object>();
        }

        public static List<object> getList(string filter)
        {
            if (!string.IsNullOrEmpty(filter))
                return convertToModel(Globals.getEFContext().SAP_SETTORE.Where(a => a.U_COD_PADRE==filter).OrderBy(c => c.U_COD_SETTORE).ToList());
            else
                return convertToModel(Globals.getEFContext().SAP_SETTORE.OrderBy(a => a.U_COD_SETTORE).ToList());
        }
    }
}