﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OrdiniSDG.Classes;
using System.Configuration;

namespace OrdiniSDG.Classes.EF
{
    public partial class SAP_Destinazioni
    {
        //Creo un oggetto fasullo per potelo popolare e aggiungere un'opzione custom per gestire 
        //l'indirizzo libero
        private class objectFake : object
        {
            public string ShippingAddressCode;
            public string ShippingAddressDesc;
            public string Street;
            public string Prov;
            public string CustomerCode;
            public string City;
            public string Country;
            public string StreetNo;
            public string CapCode;
        }


        private static List<object> convertToModel(List<SAP_Destinazioni> list)
        {
            List<object> listaTmp = new List<object>();

            listaTmp.Add(new objectFake
            {
                ShippingAddressCode = ConfigurationManager.AppSettings["StringAddressCode"],
                ShippingAddressDesc = ConfigurationManager.AppSettings["StringAddressDefault"],
                Street = "",
                Prov = "",
                CustomerCode = "",
                City = "",
                Country = "",
                StreetNo = "",
                CapCode = "",
            });

           foreach(SAP_Destinazioni s in list)
           {
               listaTmp.Add(new objectFake
               {
                   ShippingAddressCode = s.Address,
                   ShippingAddressDesc = s.City + " - " + s.Street,
                   Street = s.Street,
                   Prov = s.Prov,
                   CustomerCode = s.CardCode,
                   City = s.City,
                   Country = s.Country,
                   StreetNo = s.StreetNo,
                   CapCode = s.ZipCode
               });
           }

            return listaTmp;
        }
        public static List<object> getList(string cliCod, string filter = null)
        {
            if (filter == null)
                return convertToModel(Globals.getEFContext().SAP_Destinazioni.Where(c => c.CardCode == cliCod).OrderBy(c => c.CardCode).ToList());
            else
                return convertToModel(Globals.getEFContext().SAP_Destinazioni.Where(c => c.CardCode == cliCod && (c.Prov + " - " + c.Street).Contains(filter)).OrderBy(c => c.CardCode).ToList());
        }
    }
}