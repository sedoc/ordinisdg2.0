﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OrdiniSDG.Classes;

namespace OrdiniSDG.Classes.EF
{
    public partial class ListinoComputerGross
    {
        public OrdiniSDGEntities currentContext;

        public static List<object> getList(int rowNum = 100)
        {
            return Globals.getEFContext().ListinoComputerGross.OrderBy(c => c.Codice).Take(rowNum).ToList<object>();
        }

        public static object getItem(string id)
        {
            return Globals.getEFContext().ListinoComputerGross.Where(i => i.Codice == id).FirstOrDefault();
        }

        public static List<object> searchItem(string filter, int rowNum = 100)
        {
            if (!string.IsNullOrEmpty(filter))
                return Globals.getEFContext().ListinoComputerGross.Where(i => i.Codice.Contains(filter) || i.Descrizione.Contains(filter)).Take(rowNum).ToList<object>();
            else
                return getList();
        }
    }
}