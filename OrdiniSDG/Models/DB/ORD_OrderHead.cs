﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Configuration;
using System.Web;
using OrdiniSDG.Classes;

namespace OrdiniSDG.Classes.EF
{
    public partial class ORD_OrderHead
    {
        public OrdiniSDGEntities currentContext;

        public static string orderUpdateSapId()
        {
            OrdiniSDGEntities context = Globals.getEFContext();
            string res = "";
            try
            {
                List<SAP_Ordini_clienti> sapIDs = context.SAP_Ordini_clienti.Where(o=>o.SAP_ORD_ID ==null || o.SAP_ORD_ID==0).ToList();

                foreach (SAP_Ordini_clienti sapId in sapIDs)
                {
                    ORD_OrderHead ord = context.ORD_OrderHead.Where(o => o.ORD_ID.ToString() == sapId.IdWebOrd).FirstOrDefault();

                    ord.SAP_ORD_ID = sapId.IdSap;

                    context.SaveChanges();

                    res += ord.SAP_ORD_ID + ",";
                }
                return ("Aggiornati : "+res);
            }
            catch (Exception ex)
            {
                return ("Errore : " + ex.Message);
            }
        }

        public static string orderConfirm(int orderHeadId, int userID)
        {
            OrdiniSDGEntities context = Globals.getEFContext();
            
            try
            { 
                //controllo sull'utente,cambio stato,update
                ORD_OrderHead ORD = context.ORD_OrderHead.Where(o => o.ORD_ID == orderHeadId && o.CreationUser == userID).FirstOrDefault();
                if (ORD.StateCode == 0)
                    ORD.StateCode = 1;
                ORD.ModificationDate = DateTime.Now;
                ORD.ModificationUser = userID;
                //Utility.sendEmail((int)ORD.SAP_ORD_ID, ORD.CustomerDescription.ToString(), ORD.ORD_ID, ORD.TechnicalNote /*ORD.BackOfficeNote*/);

                //Salvo composizione dei dettagli del kit e del suo costo al momento della conferma ordine
                var detKitList = ORD.OPR_OrderDetail.Where(det => det.KitHeadCode != null);
                foreach (OPR_OrderDetail d in detKitList)
                    OPR_KID_ODetail_KDetail.create(d.OPR_ID,d.OrderHeadCode , (int)d.KitHeadCode);

                context.SaveChanges();
                
                return ("Confermato");
            }

            catch (Exception ex)
            {
                return ("Errore : "+ex.Message);
            }
        }


        public static string OrderApprove(int orderHeadId)
        {
            OrdiniSDGEntities context = Globals.getEFContext();

            try
            {
                //controllo sull'utente,cambio stato,update
                ORD_OrderHead ORD = context.ORD_OrderHead.Where(o => o.ORD_ID == orderHeadId && o.StateCode == 1).FirstOrDefault();
                //if (ORD.StateCode == 1)
                    ORD.StateCode = 2;
                // ORD.ApproveDate = DateTime.Now;
                //ORD.ApproveUser = Convert.ToInt32(ORD.AgentCode);

                Utility.sendEmail((int)ORD.SAP_ORD_ID, ORD.CustomerDescription.ToString(), ORD.ORD_ID, ORD.TechnicalNote /*ORD.BackOfficeNote*/);

                //Salvo composizione dei dettagli del kit e del suo costo al momento della conferma ordine
                var detKitList = ORD.OPR_OrderDetail.Where(det => det.KitHeadCode != null);
                foreach (OPR_OrderDetail d in detKitList)
                    OPR_KID_ODetail_KDetail.create(d.OPR_ID, d.OrderHeadCode, (int)d.KitHeadCode);

                context.SaveChanges();

                return ("Approvato");
            }
            catch (Exception ex)
            {
                return ("Errore : " + ex.Message);
            }
        }

        /*public static List<object> getOrderHead(int orderState, int userID,string date,string cusordid, string sapFilter)
        {
            List<ORD_OrderHead> res = new List<ORD_OrderHead>();

            if (Globals.getEFContext().USR_User.Where(u => u.USR_ID == userID).FirstOrDefault().Role == 2) 
            { 
                res = Globals.getEFContext().ORD_OrderHead
                    .Where(r => r.StateCode == orderState && r.CreationUser == userID)
                    .Where(r => r.AnnulDate == null)
                    .ToList();
            }
            else {
                // TECHADMIN RULES
                if (Globals.getEFContext().USR_User.Where(u => u.USR_ID == userID).FirstOrDefault().Role == 4)
                {
                    if (orderState != 3)
                    {
                        return new List<object>();
                    }
                }

                res = Globals.getEFContext().ORD_OrderHead
                    .Where(r => r.StateCode == orderState)
                     .Where(r => r.AnnulDate == null).ToList();
            }

            if (date != "")
                res = res.Where(r=>(r.ModificationDate.Value.Year == int.Parse(date.Split('-')[1]) && (r.ModificationDate.Value.Month == int.Parse(date.Split('-')[0])))).ToList();

            if(cusordid!="")
                res = res.Where(r=>r.CustomerCode.Contains(cusordid) || r.CustomerDescription.ToLower().Contains(cusordid.ToLower()) || r.ORD_ID.ToString()==cusordid || r.SAP_ORD_ID.ToString()==cusordid).ToList();

            if(!String.IsNullOrEmpty(sapFilter))
            {
                if(sapFilter == "1") //quindi solo gli ordini passati a SAP
                    res = res.Where(r => r.SAP_ORD_ID != 0 && r.SAP_ORD_ID != null).ToList();

                if(sapFilter == "2") //quindi solo gli ordini non ancora passati a SAP e quindi sul WEB
                    res = res.Where(r => r.SAP_ORD_ID == 0 || r.SAP_ORD_ID == null).ToList();
            }

            return res.OrderByDescending(r => r.ORD_ID).ToList<object>();
        }*/

        public static object create(ORD_OrderHead orderH, int userid)
        {
            OrdiniSDGEntities context = Globals.getEFContext();
            
            try
            {

                //Prendo dati aggiuntivi che non provengono direttamente dal client
                /*if (!string.IsNullOrEmpty(orderH.ShippingAddressCode))
                {
                    SAP_Destinazioni dest = context.SAP_Destinazioni.Where(d => d.Address == orderH.ShippingAddressCode && d.CardCode == orderH.CustomerCode).FirstOrDefault();
                    orderH.ShippingAddressCode = dest.Address;
                    orderH.ShippingAddressDesc = dest.Street;
                    orderH.ShippingLocality = dest.City;
                    orderH.ShippingNation = dest.Country;
                    orderH.ShippingCap = dest.ZipCode;
                    orderH.ShippingProvince = dest.Prov;
                }*/

                orderH.ShippingAddressCode = orderH.ShippingAddressCode;
                orderH.ShippingAddressDesc = orderH.ShippingAddressDesc;
                orderH.ShippingLocality = orderH.ShippingLocality;
                orderH.ShippingNation = orderH.ShippingNation;
                orderH.ShippingCap = orderH.ShippingCap;
                orderH.ShippingProvince = orderH.ShippingProvince;
                orderH.OfferNumber = orderH.OfferNumber;

                //Se lo ShippingAddressCode scelto è uguale a quello impostato nel webConfgi, e quindi uno fittizio significa
                //che il commerciale mi sta inserendo un indirizzo "fasullo"
                if (ConfigurationManager.AppSettings["StringAddressCode"] == orderH.ShippingAddressCode)
                {
                    orderH.CustomAdress = 1;
                }
                else
                {
                    orderH.CustomAdress = 0;
                }

                SAP_Clienti cli = context.SAP_Clienti.Where(c => c.CardCode == orderH.CustomerCode).FirstOrDefault();
                //orderH.ORD_ID = null;

                if (string.IsNullOrEmpty(orderH.PayCondCode))
                    orderH.PayCondCode = cli.GroupNum.ToString();

                if (string.IsNullOrEmpty(orderH.PayMethCode))
                    orderH.PayMethCode = cli.PymCode;

                /// per visualizzare l'Agent Code in fase di creazione ordine cambiata condizione 
                /// precedente condizione  if (orderH.AgentCode == null)
                if (orderH.AgentCode != null)
                    orderH.AgentCode = userid.ToString();
                

                orderH.AgentSapCode = USR_User.getIdSapByUserId(userid);

                orderH.CustomerDescription = cli.CardName;
                orderH.CreationDate = DateTime.Now;
                orderH.ModificationDate = DateTime.Now;
                orderH.CreationUser = userid;

                orderH.ReadedByErp = null; //la data di lettura deve essere vuota. la kendo grid la popola di default
                
                context.ORD_OrderHead.Add(orderH);
                
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            // Per avere il Nome dell'Agente in fase di creazione Restiutisco l'ID ORD dalla Vista
            var viewOrder = context.vw_OrderHead.Where(a => a.ORD_ID  == orderH.ORD_ID).FirstOrDefault();
            return viewOrder;
        }

        public static object update(ORD_OrderHead orderH, int userid)
        {
            OrdiniSDGEntities context = Globals.getEFContext();
            //Prendo l'oggetto dal db per averlo con un context
            orderH.OPR_OrderDetail = null;
            ORD_OrderHead ORD = context.ORD_OrderHead.Where(o => o.ORD_ID == orderH.ORD_ID).FirstOrDefault();
            Utility.CopyProperties(orderH, ORD);

            //Se lo ShippingAddressCode scelto è uguale a quello impostato nel webConfgi, e quindi uno fittizio significa
            //che il commerciale mi sta inserendo un indirizzo "fasullo"
            if (ConfigurationManager.AppSettings["StringAddressCode"] == orderH.ShippingAddressCode)
            {
                ORD.CustomAdress = 1;
            }
            else
            {
                ORD.CustomAdress = 0;
            }

            try
            {
                //Prendo dati aggiuntivi che non provengono direttamente dal client
                /*if (!string.IsNullOrEmpty(orderH.ShippingAddressCode))
                {
                    SAP_Destinazioni dest = context.SAP_Destinazioni.Where(d => d.Address == orderH.ShippingAddressCode && d.CardCode == orderH.CustomerCode).FirstOrDefault();
                    ORD.ShippingAddressCode = dest.Address;
                    ORD.ShippingAddressDesc = dest.Street;
                    ORD.ShippingLocality = dest.City;
                    ORD.ShippingNation = dest.Country;
                    ORD.ShippingCap = dest.ZipCode;
                    ORD.ShippingProvince = dest.Prov;
                }*/

                ORD.ShippingAddressCode = ORD.ShippingAddressCode;
                ORD.ShippingAddressDesc = ORD.ShippingAddressDesc;
                ORD.ShippingLocality = ORD.ShippingLocality;
                ORD.ShippingNation = ORD.ShippingNation;
                ORD.ShippingCap = ORD.ShippingCap;
                ORD.ShippingProvince = ORD.ShippingProvince;

                SAP_Clienti cli = context.SAP_Clienti.Where(c => c.CardCode == orderH.CustomerCode).FirstOrDefault();

                if (string.IsNullOrEmpty(ORD.PayCondCode))
                    ORD.PayCondCode = cli.GroupNum.ToString();


                if (string.IsNullOrEmpty(ORD.PayMethCode))
                    ORD.PayMethCode = cli.PymCode;

                if (ORD.Discount == null)
                    ORD.Discount = 0;

                ORD.CustomerCode = cli.CardCode;
                ORD.CustomerDescription = cli.CardName;
                ORD.TotalAmount = ORD.OPR_OrderDetail.Where(det => det.AnnulDate == null).Sum(det => det.DetailTotalAmount);
                ORD.TotAcquisto = ORD.OPR_OrderDetail.Where(det => det.AnnulDate == null).Sum(det => det.TotalUnitCost);

                //applico sconto e calcolo guadagno
                if (ORD.TotalAmount > 0)
                {
                    //ORD.TotalAmount = ORD.TotalAmount - ((ORD.TotalAmount / 100) * ORD.Discount);//sconto  non più utilizzato
                    ORD.Gain = 100 - ((ORD.TotAcquisto / ORD.TotalAmount) * 100);
                }
                ORD.ModificationDate = DateTime.Now;
                ORD.ModificationUser = userid;

                orderH.OfferNumber = orderH.OfferNumber;

                orderH.ReadedByErp = null; //la data di lettura deve essere vuota. la kendo grid la popola di default

                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            // Per avere il Nome dell'Agente in fase di aggiornamento restiutisco l'ID ORD dalla Vista
            var viewOrder = context.vw_OrderHead.Where(a => a.ORD_ID == orderH.ORD_ID).FirstOrDefault();
            return viewOrder;
        }

        public static object delete(ORD_OrderHead orderH, int userid)
        {
            OrdiniSDGEntities context = Globals.getEFContext();

            //Prendo l'oggetto dal db per averlo con un context
            ORD_OrderHead ORD = context.ORD_OrderHead.Where(o => o.ORD_ID == orderH.ORD_ID).FirstOrDefault();
            try
            {
                //context.OPR_OrderDetail.RemoveRange(ORD.OPR_OrderDetail);
                //context.ORD_OrderHead.Remove(ORD);
                ORD.AnnulDate = DateTime.Now;
                ORD.AnnulUser = userid;

                foreach (OPR_OrderDetail row in ORD.OPR_OrderDetail )
                {
                    row.AnnulDate = DateTime.Now;
                    row.AnnulUser = userid;
                }

                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ORD;
        }
    }
}