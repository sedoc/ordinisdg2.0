﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OrdiniSDG.Classes;

namespace OrdiniSDG.Classes.EF
{
    public partial class vw_OrderHead
    {

        public OrdiniSDGEntities currentContext;

        public static List<object> getOrderHead(int orderState, int userID,string date,string cusordid, string sapFilter)
         {
             List<vw_OrderHead> res = new List<vw_OrderHead>();

             if (Globals.getEFContext().USR_User.Where(u => u.USR_ID == userID).FirstOrDefault().Role == 2) //Se il ruolo e' Agent
             { 
                 res = Globals.getEFContext().vw_OrderHead
                     .Where(r => r.StateCode == orderState && r.CreationUser == userID)
                     .Where(r => r.AnnulDate == null)
                     .ToList();
             }
             else {
                 // TECHADMIN RULES
                 if (Globals.getEFContext().USR_User.Where(u => u.USR_ID == userID).FirstOrDefault().Role == 4)
                 {
                     if (orderState != 3)
                     {
                         return new List<object>();
                     }
                 }

                 res = Globals.getEFContext().vw_OrderHead
                     .Where(r => r.StateCode == orderState)
                      .Where(r => r.AnnulDate == null).ToList();
             }

             if (date != "")
                 res = res.Where(r=>(r.ModificationDate.Value.Year == int.Parse(date.Split('-')[1]) && (r.ModificationDate.Value.Month == int.Parse(date.Split('-')[0])))).ToList();

             if(cusordid!="")
                 res = res.Where(r=>r.CustomerCode.Contains(cusordid) || r.CustomerDescription.ToLower().Contains(cusordid.ToLower()) || r.ORD_ID.ToString()==cusordid || r.SAP_ORD_ID.ToString()==cusordid).ToList();

             if(!String.IsNullOrEmpty(sapFilter))
             {
                 if(sapFilter == "1") //quindi solo gli ordini passati a SAP
                     res = res.Where(r => r.SAP_ORD_ID != 0 && r.SAP_ORD_ID != null).ToList();

                 if(sapFilter == "2") //quindi solo gli ordini non ancora passati a SAP e quindi sul WEB
                     res = res.Where(r => r.SAP_ORD_ID == 0 || r.SAP_ORD_ID == null).ToList();
             }

             return res.OrderByDescending(r => r.ORD_ID).ToList<object>();
         }
    }
}