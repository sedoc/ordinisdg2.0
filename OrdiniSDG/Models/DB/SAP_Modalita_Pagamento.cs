﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OrdiniSDG.Classes;

namespace OrdiniSDG.Classes.EF
{
    public partial class SAP_Modalita_Pagamento
    {
        private static List<object> convertToModel(List<SAP_Modalita_Pagamento> list)
        {
            /*return list.Select(l => new {
                PayMethCode = l.PayMethCod,
                PayMethDesc = l.Descript,
                Color = "red",
            }).ToList<object>();*/
            var listItem = list.Select(l => new
            {
                PayMethCode = l.PayMethCod,
                PayMethDesc = l.Descript,
            }).ToList<object>();

            return listItem;

        }

        public static List<object> getList()
        {
            return convertToModel(Globals.getEFContext().SAP_Modalita_Pagamento.OrderBy(c => c.PayMethCod).ToList());
        }
    }
}