﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OrdiniSDG.Classes;

namespace OrdiniSDG.Classes.EF
{
    public partial class OPR_OrderDetail
    {
        public OrdiniSDGEntities currentContext;
        
        public static List<object> getOrderDetailList(int orderHeadID)
        {
            return Globals.getEFContext().OPR_OrderDetail
                .Where(d=>d.OrderHeadCode== orderHeadID)
                .Where(d => d.AnnulDate == null)
                .OrderBy(d => d.OPR_ID).ToList<object>();
        }

        public static object create(OPR_OrderDetail orderD, int userID)
        {
            OrdiniSDGEntities context = Globals.getEFContext();

            try
            {
                //Prendo dati aggiuntivi che non provengono direttamente dal client
                //SAP_Destinazioni dest = context.SAP_Destinazioni.Where(d => d.Address == orderH.ShippingAddressCode).FirstOrDefault();
                //SAP_Clienti cli = context.SAP_Clienti.Where(c => c.CardCode == orderH.CustomerCode).FirstOrDefault();
                //orderH.ORD_ID = null;

                orderD.CreationDate = DateTime.Now;
                orderD.CreationUser = userID;
                orderD.DetailTotalAmount = orderD.Quantity * orderD.UnitPrice;
                orderD.TotalUnitCost = orderD.Quantity * orderD.UnitCost;
                context.OPR_OrderDetail.Add(orderD);
                orderD.NewArtCode = (SAP_Articoli.getItem(orderD.ArtCode) == null);
                context.SaveChanges();
                //Riprendo la testata per rifare alcuni calcoli che dipendono dalle righe TODO
                ORD_OrderHead ORD = context.ORD_OrderHead.Where(o => o.ORD_ID == orderD.OrderHeadCode).First();
                ORD.TotalAmount = ORD.OPR_OrderDetail.Where(det => det.AnnulDate == null).Sum(det => det.DetailTotalAmount);
                ORD.TotAcquisto = ORD.OPR_OrderDetail.Where(det => det.AnnulDate == null).Sum(det => det.TotalUnitCost);
                //applico sconto e calcolo guadagno
                if (ORD.TotalAmount > 0)
                {
                    //ORD.TotalAmount = ORD.TotalAmount - ((ORD.TotalAmount / 100) * ORD.Discount);//sconto  non più utilizzato
                    ORD.Gain = 100 - ((ORD.TotAcquisto / ORD.TotalAmount) * 100);
                }
                
                context.SaveChanges();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return orderD;
        }

        public static object update(OPR_OrderDetail orderD,  int userID)
        {
            OrdiniSDGEntities context = Globals.getEFContext();
            //Prendo l'oggetto dal db per averlo con un context

            OPR_OrderDetail DET = context.OPR_OrderDetail.Where(o => o.OPR_ID == orderD.OPR_ID).FirstOrDefault();

            Utility.CopyProperties(orderD, DET);
            //perchè altrimenti duplica la riga di testata
            DET.ORD_OrderHead = null;
            try
            {
                //Prendo dati aggiuntivi che non provengono direttamente dal client e li risetto
                //SAP_Destinazioni dest = context.SAP_Destinazioni.Where(d => d.Address == orderD.ShippingAddressCode).FirstOrDefault();
                //SAP_Clienti cli = context.SAP_Clienti.Where(c => c.CardCode == orderH.CustomerCode).FirstOrDefault();
                DET.ModificationDate = DateTime.Now;
                DET.ModificationUser = userID;
                DET.DetailTotalAmount = DET.Quantity * DET.UnitPrice;
                DET.TotalUnitCost = DET.Quantity * DET.UnitCost;
                DET.NewArtCode = (SAP_Articoli.getItem(DET.ArtCode) == null);
                context.SaveChanges();

                //Riprendo la testata per rifare alcuni calcoli che dipendono dalle righe TODO
                ORD_OrderHead ORD = context.ORD_OrderHead.Where(o => o.ORD_ID == orderD.OrderHeadCode).First();
                ORD.TotalAmount = ORD.OPR_OrderDetail.Where(det => det.AnnulDate == null).Sum(det => det.DetailTotalAmount);
                ORD.TotAcquisto = ORD.OPR_OrderDetail.Where(det => det.AnnulDate == null).Sum(det => det.TotalUnitCost);
                //applico sconto e calcolo guadagno
                if (ORD.TotalAmount > 0)
                {
                    //ORD.TotalAmount = ORD.TotalAmount - ((ORD.TotalAmount / 100) * ORD.Discount); //sconto  non più utilizzato
                    ORD.Gain = 100 - ((ORD.TotAcquisto / ORD.TotalAmount) * 100);
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return DET;
        }

        public static object delete(OPR_OrderDetail orderD, int userID)
        { 
            OrdiniSDGEntities context = Globals.getEFContext();

            //Prendo l'oggetto dal db per averlo con un context
            OPR_OrderDetail DET = context.OPR_OrderDetail.Where(o => o.OPR_ID == orderD.OPR_ID).FirstOrDefault();
            try
            {
                //context.OPR_OrderDetail.Remove(DET);

                DET.AnnulDate = DateTime.Now;
                DET.AnnulUser = userID;

                context.SaveChanges();

                //Riprendo la testata per rifare alcuni calcoli che dipendono dalle righe TODO
                ORD_OrderHead ORD = context.ORD_OrderHead.Where(o => o.ORD_ID == orderD.OrderHeadCode).First();
                ORD.TotalAmount = ORD.OPR_OrderDetail.Where(det => det.AnnulDate == null).Sum(det => det.DetailTotalAmount);
                ORD.TotAcquisto = ORD.OPR_OrderDetail.Where(det => det.AnnulDate == null).Sum(det => det.TotalUnitCost);
                //applico sconto e calcolo guadagno
                if(ORD.TotalAmount ==0)
                {
                    ORD.Gain = 0;
                }
                else if (ORD.TotalAmount >= 0)
                {
                    //ORD.TotalAmount = ORD.TotalAmount - ((ORD.TotalAmount / 100) * ORD.Discount); //sconto  non più utilizzato
                    ORD.Gain = 100 - ((ORD.TotAcquisto / ORD.TotalAmount) * 100);
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return DET;
        }

        //Aggiorno i costi nei dettagli e testate degli ordini in bozza alla modifica di un kit 
        public static bool updateCosts(int kitHeadCode)
        {
            try
            {
                OrdiniSDGEntities context = Globals.getEFContext();
                
                var detailList = context.OPR_OrderDetail.Where(d => d.KitHeadCode == kitHeadCode).ToList();
                if (detailList.Any())
                {
                    foreach (OPR_OrderDetail det in detailList)
                    {
                        if (det.ORD_OrderHead.StateCode==0 && det.ORD_OrderHead.AnnulDate==null)
                        {
                            vw_KitHead kih = context.vw_KitHead.Where(k => k.KIH_ID == kitHeadCode).FirstOrDefault();
                            det.DetailTotalAmount = det.Quantity * kih.kitHeadCost;
                            det.UnitCost = (decimal)kih.kitHeadCost;
                            context.SaveChanges();
                            //Aggiorno testata
                            ORD_OrderHead ORD = det.ORD_OrderHead;
                            ORD.ModificationDate = DateTime.Now;
                            ORD.TotalAmount = ORD.OPR_OrderDetail.Sum(d => d.DetailTotalAmount);
                            //applico sconto
                            if (ORD.TotalAmount > 0)
                            {
                                //ORD.TotalAmount = ORD.TotalAmount - ((ORD.TotalAmount / 100) * ORD.Discount);
                                ORD.Gain = 100 - ((ORD.TotAcquisto / ORD.TotalAmount) * 100);
                            }
                            context.SaveChanges();
                        }
                    }
                }
                
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}