﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using OrdiniSDG.Models.DB;
using OrdiniSDG.Repositories;

namespace OrdiniSDG.Classes.EF
{
    public partial class USR_User : IAccount
    {


        public OrdiniSDGEntities currentContext;


        public List<USR_UserViewModel> getUsers()
        {
            try
            {
                var context = Globals.getEFContext();

                List<USR_UserViewModel> users = context.USR_User.Where(u => u.USR_ID > 0).Select(x => new USR_UserViewModel
                {
                    USR_ID = x.USR_ID,
                    Email = x.Email,
                    Username = x.Username,
                    Name = x.Name,
                    Surname = x.Surname,
                    USR_ID_SAP = x.USR_ID_SAP,
                    CreationDate = x.CreationDate,
                    DeletingDate = x.DeletingDate,
                    LastLoginDate = x.LastLoginDate,
                    GuidRestorePass = x.GuidRestorePass,
                    LastPasswordChangedDate = x.LastPasswordChangedDate,
                    Password = x.Password,
                    Role = x.Role,
                    UserModify = x.UserModify,
                    VerifDateGuidRP = x.VerifDateGuidRP,
                    RoleName = x.RLE_Role.Description
                }).ToList();
                
                return users;
            }
            catch (Exception Ex)
            {
                return null;
            }
        }
        public static USR_User getUserById(int id)
        {
            try
            {
                var context = Globals.getEFContext();

                USR_User user = context.USR_User.Where(u => u.USR_ID == id).FirstOrDefault();

                if (user != null)
                {
                    user.currentContext = context;
                }

                return user;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static USR_User getUserFromEmail(string email)
        {
            try
            {
                OrdiniSDGEntities context = Globals.getEFContext();

                USR_User user = context.USR_User.Where(a => a.Email == email).FirstOrDefault<USR_User>();

                if (user != null)
                {
                    user.currentContext = context;
                }

                return user;
            }
            catch
            {
                return null;
            }
            
        }

        public USR_User getUserFromUsername(string username)
        {
            try
            {
                OrdiniSDGEntities context = Globals.getEFContext();

                USR_User user = context.USR_User.Where(a => a.Username == username).FirstOrDefault<USR_User>();

                if (user != null)
                {
                    user.currentContext = context;
                }

                return user;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static string getIdSapByUserId(int userid)
        {
            string AgentNull = "-1";
            try
            {
                var context = Globals.getEFContext();

                var userIdSap = context.USR_User.Where(u => u.USR_ID == userid).FirstOrDefault().USR_ID_SAP;
                
                return userIdSap.ToString();
            }
            catch (Exception ex)
            {
                return AgentNull;//Concordato con Gabriele di SAP in caso non ci sia un utente corrispondente
            }
        }

        public USR_User addUser(USR_User user)
        {
            try
            {
                OrdiniSDGEntities context = Globals.getEFContext();
                if (context.USR_User.Any(x => x.Username == user.Username))
                {
                    throw new Exception("User name is exitst");
                }
                user.Password = PasswordHash.CreateHash(user.Password);

                USR_User userResult = null;
                if (user != null)
                {
                    userResult = context.USR_User.Add(user);
                    context.SaveChanges();
                }

                return userResult;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<RLE_RoleViewModel> getRoles()
        {
            try
            {
                var context = Globals.getEFContext();

                List<RLE_RoleViewModel> roles = context.RLE_Role.Select(x => new RLE_RoleViewModel
                {
                    RLE_ROLE1 = x.RLE_ROLE1,
                    Description = x.Description,
                }).ToList();

                return roles;
            }
            catch (Exception Ex)
            {
                return null;
            }
        }

        public USR_User updateUser(USR_User user)
        {
            try
            {
                OrdiniSDGEntities context = Globals.getEFContext();
                var oldUser = context.USR_User.FirstOrDefault(x => x.USR_ID == user.USR_ID);
                if (oldUser.Password != user.Password)
                {
                    user.Password = PasswordHash.CreateHash(user.Password);
                }
                if (user != null)
                {
                    oldUser.Email = user.Email;
                    oldUser.Name = user.Name;
                    oldUser.Password = user.Password;
                    oldUser.Role = user.Role;
                    oldUser.Surname = user.Surname;
                    oldUser.Username = oldUser.Username;
                    context.SaveChanges();
                }

                return user;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool isExistUserName(string userName)
        {
            try
            {
                OrdiniSDGEntities context = Globals.getEFContext();
                
                return context.USR_User.Any(user => user.Username == userName);
            }
            catch (Exception ex)
            {
                return true;
            }
        }
    }
}