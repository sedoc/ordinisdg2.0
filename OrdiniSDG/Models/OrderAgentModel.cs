﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrdiniSDG.Models
{
    public class OrderAgentModel
    {
        public decimal? TotalAmount { get; set; }
        public decimal? Gain { get; set; }
        public string AgentId { get; set; }
        public string AgentName { get; set; }
        public int ORD_ID { get; set; }
        public Nullable<System.DateTime> ReadedByErp { get; set; }
        public string CustomerDescription { get; set; }
        public Nullable<int> SAP_ORD_ID { get; set; }
    }
}