﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using System.Configuration;
using System.Net.Mail;
using System.ServiceModel.Channels;
using OrdiniSDG.Classes.EF;

namespace OrdiniSDG.Classes
{
    public class Utility
    {
        /// <summary>
        /// Copies the properties to a destination object.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="destination">The destination.</param>
        public static void CopyProperties(object source, object destination)
        {
            // If any this null throw an exception
            if (source == null || destination == null)
                throw new Exception("Source or/and Destination Objects are null");
            // Getting the Types of the objects
            Type typeDest = destination.GetType();
            Type typeSrc = source.GetType();
            // Collect all the valid properties to map
            var results = from srcProp in typeSrc.GetProperties()
                          let targetProperty = typeDest.GetProperty(srcProp.Name)
                          where srcProp.CanRead
                          && targetProperty != null
                          && (targetProperty.GetSetMethod(true) != null && !targetProperty.GetSetMethod(true).IsPrivate)
                          && (targetProperty.GetSetMethod().Attributes & MethodAttributes.Static) == 0
                          && targetProperty.PropertyType.IsAssignableFrom(srcProp.PropertyType)
                          select new { sourceProperty = srcProp, targetProperty = targetProperty };
            //map the properties
            foreach (var props in results)
            {
                props.targetProperty.SetValue(destination, props.sourceProperty.GetValue(source, null), null);
            }
        }

        /// <summary>
        /// Invio la mail di conferma per l'utente tecnico
        /// </summary>
        /// <param name="sapId"></param>
        /// <param name="customerDescriptiom"></param>
        /// <param name="ordId"></param>
        /// <param name="note"></param>
        /// <param name="backOfficeNote"></param>
        public static void sendEmail(int sapId, string customerDescription, int ordId, string note /*string backOfficeNote*/)
        {
            try
            {
                int Enable = Int32.Parse(ConfigurationManager.AppSettings["EnableMailService"]);
                if (Enable == 1)
                {
                    SmtpClient SmtpClient = new SmtpClient();
                    MailMessage Message = new MailMessage();

                    //Faccio uno split del campo del web config poichè potrebbero esistere più indirizzi mail
                    String[] address = ConfigurationManager.AppSettings["Mailto"].Split(';');

                    foreach (String a in address)
                        Message.To.Add(new MailAddress(a));

                    Message.Subject = "Note Tecniche Ordine " + sapId + " -- " + customerDescription;
                    Message.Body = "Id Ordine Web " + ordId + "\n";
                    Message.Body += "Note tecniche: " + note + "\n";
                    //Message.Body += "Note backOffice: " + backOfficeNote;

                    if (note != null)
                        SmtpClient.Send(Message);
                }
            }
            catch { }
        }
    }
}