﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OrdiniSDG.Mailers;
using OrdiniSDG.Classes.EF;
using System.Reflection;

namespace OrdiniSDG.Classes
{
    public class Globals
    {
        public const string sessionUserIdKey = "USR_ID";
        public const string sessionRoleIdKey = "Role";
        public const string uploadFile = "Upload/";
        public const int cookiesDurationDays = 10;


        public static OrdiniSDGEntities getEFContext()
        {
            try
            {
                return new OrdiniSDGEntities();
            }
            catch
            {
                return null;
            }
        }

        public static string getCurrentCulture()
        {
            try
            {
                return HttpContext.Current.Request.Cookies["_culture"].Value.ToString();
            }
            catch (Exception e)
            {
                return "";
            }
        }

        public static UserMailers getMailer()
        {
            try
            {
                return new UserMailers();
            }
            catch
            {
                return null;
            }
        }

        
    }
}