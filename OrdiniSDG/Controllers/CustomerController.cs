﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Reflection;
using System.Web.Mvc;
using OrdiniSDG.Classes.EF;
using Newtonsoft.Json;
using Microsoft.AspNet.Identity;

namespace OrdiniSDG.Controllers
{
    public class CustomerController : BaseController
    {
        // GET: Customer
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public JsonResult Read(string filter=null)
        {
            return Json(SAP_Clienti.getList(filter), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ReadAddresses(string codCli, string filter=null)
        {
            return Json(SAP_Destinazioni.getList(codCli,filter), JsonRequestBehavior.AllowGet);
        }
        public JsonResult ReadPayMeth()
        {
            return Json(SAP_Modalita_Pagamento.getList(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult ReadPayCond(string filter = null)
        {
            return Json(SAP_Condizione_Pagamento.getList(filter), JsonRequestBehavior.AllowGet);
        }
    }
}