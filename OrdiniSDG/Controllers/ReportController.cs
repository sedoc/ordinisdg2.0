﻿using OrdiniSDG.Classes.EF;
using OrdiniSDG.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web.Mvc;

namespace OrdiniSDG.Controllers
{
    public class ReportController : Controller
    {
        private readonly OrdiniSDGEntities context;
        private List<OrderPiece> OtherOrders = new List<OrderPiece>();
        private List<OrderPiece> ServiceOrders = new List<OrderPiece>();

        public ReportController()
        {
            context = new OrdiniSDGEntities();
        }

        /// <summary>
        /// Invio di una lettera con un rapporto sugli ordini per le ultime 24 ore
        /// </summary>
        /*public void SendDailyReport()
        {
            try
            {
                DateTime startDateTime = DateTime.Today; //Today at 00:00:00
                DateTime endDateTime = DateTime.Today.AddDays(1).AddTicks(-1); //Today at 23:59:59

                //used for test
                //DateTime startDateTime = new DateTime(2018, 7, 3, 00, 00, 00);
                //DateTime endDateTime = new DateTime(2018, 7, 3, 23, 59, 59);

                var ORDs = GetOrdersWithin(startDateTime, endDateTime);
                SetServiceAndOthersOrders(ORDs);
                string sender = new MailAddress(ConfigurationManager.AppSettings["smtpUser"]).ToString();

                SmtpClient SmtpClient = InitSmtpClient(sender);
                MailMessage Message = InitDailyMailMessage(sender);
                SetMailTo(Message);

                Message.IsBodyHtml = true;
                SmtpClient.Send(Message);
            }
            catch { }
        }*/

        public void BaseReport(DateTime startDateTime, DateTime endDateTime, string period)
        {
            try
            {
                var ORDs = SelectOrdersByAgent(startDateTime, endDateTime);
                ServiceandOthersOrdersByAgent(ORDs);
                string sender = new MailAddress(ConfigurationManager.AppSettings["smtpUser"]).ToString();

                SmtpClient SmtpClient = InitSmtpClient(sender);
                MailMessage Message = InitDailyMailMessage(sender, startDateTime, endDateTime, period);
                SetMailTo(Message);

                Message.IsBodyHtml = true;
                SmtpClient.Send(Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        public void SendDailyReport()
        {
           
            DateTime startDateTime = DateTime.Today; //Today at 00:00:00
            DateTime endDateTime = DateTime.Today.AddDays(1).AddTicks(-1); //Today at 23:59:59

            //used for test
            //DateTime startDateTime = new DateTime(2021, 4, 28, 00, 00, 00);
            //DateTime endDateTime = new DateTime(2021, 4, 28, 23, 59, 59);
            
            BaseReport(startDateTime, endDateTime, "Giornaliero");
        }
        
        public void SendWeeklyReport()
        {
            DateTime startDateTime = DateTime.Today.AddDays(-7).AddTicks(-1); //Last week in this day at 23:59:00
            DateTime endDateTime = DateTime.Today.AddTicks(-1); //Today at 00:00:00

            //used for test
            //DateTime startDateTime = new DateTime(2021, 4, 5, 00, 00, 00);
            //DateTime endDateTime = new DateTime(2021, 4, 10, 23, 59, 59);
            
            BaseReport(startDateTime, endDateTime, "Settimanale");
        }
        
        public void SendMonthlyReport()
        {
            DateTime startDateTime = DateTime.Today.AddMonths(-1).AddTicks(-1); //Last month at 23:59:59
            DateTime endDateTime = DateTime.Today.AddTicks(-1); //Today at 23:59:59

            //used for test
            //DateTime startDateTime = new DateTime(2021, 03, 1, 0, 00, 00);
            //DateTime endDateTime = new DateTime(2021, 03, 31, 23, 59, 00);
            
            BaseReport(startDateTime, endDateTime, "Mensile");
        }
        
        public void SendQuarterlyReport()
        {
            //DateTime startDateTime = DateTime.Today.AddMonths(-3).AddTicks(-1); //Last 3 months at 00:00:00
            //DateTime endDateTime = DateTime.Today.AddDays(1).AddTicks(-1); //Today at 23:59:59

            //used for test
            DateTime startDateTime = new DateTime(2021, 4, 1, 00, 00, 00);
            DateTime endDateTime = new DateTime(2021, 6, 30, 23, 59, 59);
            
            BaseReport(startDateTime, endDateTime, "Trimestrale");
        }
        
        public void SendAnnualReport()
        {
            DateTime startDateTime = DateTime.Today.AddYears(-1).AddTicks(-1); //Last year at 23:59:59
            DateTime endDateTime = DateTime.Today.AddDays(1).AddTicks(-1); //Today at 23:59:59

            //used for test
            //DateTime startDateTime = new DateTime(2020, 3, 31, 23, 59, 59);
            //DateTime endDateTime = new DateTime(2021, 3, 31, 23, 59, 59);
            
            BaseReport(startDateTime, endDateTime, "Annuale");
        }


        private void ServiceandOthersOrdersByAgent(List<OrderAgentModel> oRDs)
        {
            try
            {
                foreach (var i in oRDs)
                {
                    var itemCodes = context.OPR_OrderDetail.Where(x => (x.OrderHeadCode == i.ORD_ID) && (x.AnnulDate == null)).ToList();
                    //var headOrder = context.vw_OrderHead.Where(x => (x.ORD_ID == i.ORD_ID)).ToList();
                    foreach (var j in itemCodes)
                    {
                        SAP_Articoli article = context.SAP_Articoli.FirstOrDefault(x => x.ItemCode == j.ArtCode);
                        
                        if (article != null && article.U_SED_AREA_AFFARI == "SZ")
                        {
                            ServiceOrders.Add(new OrderPiece
                            {
                                CreationDate = i.ReadedByErp,
                                CustomerDescription = i.CustomerDescription,
                                Gain = i.Gain, // percentage
                                SAP_ORD_ID = i.SAP_ORD_ID,
                                TotalUnitCost = j.TotalUnitCost, //Total Unit Cost
                                DetailTotalAmount = j.DetailTotalAmount, //TotaleUnitCost + Other Costs
                                AgentName = i.AgentName,
                            });
                        }
                        else
                        {
                            OtherOrders.Add(new OrderPiece
                            {
                                CreationDate = i.ReadedByErp,
                                CustomerDescription = i.CustomerDescription,
                                Gain = i.Gain,
                                SAP_ORD_ID = i.SAP_ORD_ID,
                                DetailTotalAmount = j.DetailTotalAmount,
                                TotalUnitCost = j.TotalUnitCost,
                                AgentName = i.AgentName,
                            });
                        }
                    }
                }
            }
            catch (Exception e) { }
        }

        private void SetServiceAndOthersOrders(List<ORD_OrderHead> oRDs)
        {
            try
            {
                foreach (var i in oRDs)
                {
                    var itemCodes = context.OPR_OrderDetail.Where(x => (x.OrderHeadCode == i.ORD_ID) && (x.AnnulDate == null)).ToList();
                    foreach (var j in itemCodes)
                    {
                        SAP_Articoli article = context.SAP_Articoli.FirstOrDefault(x => x.ItemCode == j.ArtCode);

                        if (article != null && article.U_SED_AREA_AFFARI == "SZ")
                        {
                            ServiceOrders.Add(new OrderPiece
                            {
                                CreationDate = i.ReadedByErp,
                                CustomerDescription = i.CustomerDescription,
                                Gain = i.Gain,
                                SAP_ORD_ID = i.SAP_ORD_ID,
                                TotalUnitCost = j.TotalUnitCost,
                                DetailTotalAmount = j.DetailTotalAmount
                            });
                        }
                        else
                        {
                            OtherOrders.Add(new OrderPiece
                            {
                                CreationDate = i.ReadedByErp,
                                CustomerDescription = i.CustomerDescription,
                                Gain = i.Gain,
                                SAP_ORD_ID = i.SAP_ORD_ID,
                                TotalUnitCost = j.TotalUnitCost,
                                DetailTotalAmount = j.DetailTotalAmount
                            });
                        }
                    }
                }
            }
            catch (Exception e) { }
        }

        private List<SAP_Articoli> GetServiceOrders(DateTime startDateTime, DateTime endDateTime)
        {
            try
            {
                return context.SAP_Articoli.Where(x => (x.U_SED_AREA_AFFARI == "SZ") && (x.ItemCode == "00HN821")).ToList();
            }
            catch (Exception e) { }
            return context.SAP_Articoli.Where(x => (x.U_SED_AREA_AFFARI == "SZ") && (x.ItemCode == "00HN821")).ToList();
        }

        private void SetMailTo(MailMessage Message)
        {
            String[] address = ConfigurationManager.AppSettings["ReportMailto"].Split(';');

            foreach (String a in address)
                Message.To.Add(new MailAddress(a));
        }

        /// <summary>
        /// Invio di una lettera con un report degli ordini per l'ultima settimana
        /// </summary>
        

        private MailMessage InitMailWeeklyMessage(string sender, DateTime startDateTime, DateTime endDateTime)
        {
            /* versione "Daily" */
            try
            {
                if (ServiceOrders != null && OtherOrders != null)
                {
                    string otherOrders = GetOrdersTableProdotto("Ordini Prodotti", OtherOrders);
                    string serviceOrders = GetOrdersTable("Ordini Servizi", ServiceOrders);
                    string Title = @"Report Settimanale " + startDateTime.ToString("dd/MM/yyyy") +" " +startDateTime.ToString("HH:mm") + " - " + endDateTime.ToString("dd/MM/yyyy") +" "+ endDateTime.ToString("HH:mm") ;
                    string Total = @"<table style='width: 90%;'> 
                                    <caption style='border-style:solid;'>Total Orders & Services</caption>
                                <tr>
                                    <th colspan='2' style='width: 18%; text-align:left;'>Numero Totale Ordini & Servizi</th>
                                    <th style='width: 18%; text-align:left;'></th>
                                    <th style='width: 18%; text-align:left;'></th>
                                    <th style='width: 18%; text-align:left;'>Totale Prodotti & Servizi</th>
                                </tr>
                                <td colspan='2'>" + TotalNumOders +
                                   "</td><td>" +
                                   "</td><td>" +
                                   "</td><td>" + String.Format("{0,10:N}", GlobalAmount) + "€" +
                                   "</ tr ></ table >";

                    return SetMailObject(sender, Title, otherOrders, serviceOrders, Total);
                }
                return MakeEmptyMessage(sender, "Weekly");
            }
            catch (Exception e) { }
            return null;
             /*
            decimal? WeeklyOrderAmount = 0;
            decimal? WeeklyServiceAmount = 0;
            List<decimal?> gainsOrder = new List<decimal?>();
            List<decimal?> gainsServices = new List<decimal?>();

            if (ServiceOrders != null && OtherOrders != null)
            {
                foreach (var i in OtherOrders)
                {
                    WeeklyOrderAmount += i.DetailTotalAmount;
                    gainsOrder.Add(i.Gain);
                }

                foreach (var i in ServiceOrders)
                {
                    WeeklyServiceAmount += i.DetailTotalAmount;
                    gainsServices.Add(i.Gain);
                }

                //string otherOrders = "Other orders = "+ OtherOrders.Count + WeeklyTotalAmount.ToString();
                string otherOrders = @"<table style='width: 90%;'><caption style='border-style:solid;'>Totale Ordini Settimana</caption>
                                <tr>
                                    <th style='width: 30%; text-align:left;'>Numero Totale Ordini</th>
                                    <th style='width: 30%; text-align:left;'>Totale Ordini</th>
                                    <th style='width: 30%; text-align:left;'>Marginalità Ordini</th>
                                </tr>
                                <td>" + OtherOrders.Count +
                             "</td><td>" + WeeklyOrderAmount + " €" +
                            " </td><td>" + String.Format("{0,10:N}", gainsOrder.Average()) + " %" +
                            "</td></tr></table>";

                //string serviceOrders = "Service orders = "+ ServiceOrders.Count;
                string serviceOrders = @"<table style='width: 90%;'><caption style='border-style:solid;'>Totale Servizi Settimana</caption>
                                <tr>
                                    <th style='width: 30%; text-align:left;'>Numero Totale Servizi</th>
                                    <th style='width: 30%; text-align:left;'>Totale Servizi</th>
                                    <th style='width: 30%; text-align:left;'>Marginalità Servizi</th>
                                </tr>
                                <td>" + ServiceOrders.Count +
                            "</td><td>" + WeeklyServiceAmount + " €" +
                            " </td><td>" + String.Format("{0,10:N}", gainsServices.Average()) + " %" +
                            "</td></tr></table>";

                string Total = @"<table style='width: 90%;'><caption style='border-style:solid;'>Totale Ordini & Servizi Settimana</caption>
                                <tr>
                                    <th style='width: 30%; text-align:left;'>Numero Totale Ordini & Servizi</th>
                                    <th style='width: 30%; text-align:left;'>Totale Ordini & Servizi</th>
                                    <th style='width: 30%; text-align:left;'></th>
                                </tr>
                                <td>" + (OtherOrders.Count + ServiceOrders.Count) +
                                "</td><td>" + (WeeklyOrderAmount + WeeklyServiceAmount) + " €" +
                                " </td><td>"+
                                "</td></tr></table>";

                return SetMailObject(sender, "Weekly report order", otherOrders, serviceOrders, Total);
            }
            return MakeEmptyMessage(sender, "week"); */
        }

        private List<OrderAgentModel> SelectOrdersByAgent(DateTime startDateTime, DateTime endDateTime)
        {
            List<OrderAgentModel> ORDs = context.ORD_OrderHead.Join(context.SAP_Agenti,
                    orders => orders.AgentSapCode,
                    agents => agents.SlpCode, /*
                                                        * In this way, there is a bug: some orders ReadByErp on Friday but by SAP Monday, will not be counted.
                                                        * Need a history of the oreders that are not read by SAP, check if are read, insert in Monday ecc.Z       
                                                        */
                    (orders, agents) => new { ORD_OrderHead = orders, SAP_Agenti = agents })
                        .Where(x => x.ORD_OrderHead.ReadedByErp >= startDateTime && x.ORD_OrderHead.ReadedByErp <= endDateTime) //all orders within the limits
                        .Where(x => x.ORD_OrderHead.AnnulDate == null) //not deleted
                        .Where(x => x.ORD_OrderHead.SAP_ORD_ID != 0)  //readed by SAP
                        .Where(x => x.ORD_OrderHead.AgentSapCode != "-1")
                        .OrderBy(x => x.ORD_OrderHead.ReadedByErp)
                        .Select(x => new OrderAgentModel
                        {
                            TotalAmount = x.ORD_OrderHead.TotalAmount,
                            Gain = x.ORD_OrderHead.Gain,
                            AgentName = x.SAP_Agenti.SlpName,
                            AgentId = x.SAP_Agenti.SlpCode,
                            CustomerDescription=x.ORD_OrderHead.CustomerDescription,
                            ORD_ID = x.ORD_OrderHead.ORD_ID,
                            SAP_ORD_ID = x.ORD_OrderHead.SAP_ORD_ID,
                            ReadedByErp = x.ORD_OrderHead.ReadedByErp
                        }).ToList();
            return ORDs;
        }
        

        private MailMessage SetMailObject(string sender, string subject, string beginBody, string middleBody, string endBody = "")
        {
            MailMessage msg = new MailMessage()
            {
                From = new MailAddress(sender),
                Subject = subject,
                Body = beginBody + middleBody + endBody,
                IsBodyHtml = true
            };

            return msg;
        }

        private List<ORD_OrderHead> GetOrdersWithin(DateTime startDateTime, DateTime endDateTime)
        {
            List<ORD_OrderHead> ORDs = context.ORD_OrderHead
                    .Where(x => x.ReadedByErp >= startDateTime && x.ReadedByErp <= endDateTime) //all orders within the limits
                    .Where(x => x.AnnulDate == null) //not deleted
                    .Where(x => x.SAP_ORD_ID != 0)  //readed by SAP
                    .OrderBy(x => x.ReadedByErp)
                    .ToList();
            return ORDs;
        }

        private SmtpClient InitSmtpClient(string sender)
        {
            SmtpClient client = new SmtpClient()
            {
                Host = ConfigurationManager.AppSettings["smtpServer"],
                Port = Convert.ToInt32(ConfigurationManager.AppSettings["smtpPort"]),
                EnableSsl = true,
                Credentials = new System.Net.NetworkCredential(sender, ConfigurationManager.AppSettings["smtpPass"])
            };

            return client;
        }

        int TotalNumOders = 0;
        decimal? GlobalAmount = 0;
        List<decimal?> GlobalGains = new List<decimal?>();
        private MailMessage InitDailyMailMessage(string sender, DateTime startDateTime, DateTime endDateTime, string period)
        {
            try
            {
                if (ServiceOrders != null && OtherOrders != null)
                {
                    string otherOrders = GetOrdersTableProdotto("Ordini Prodotti", OtherOrders);
                    string serviceOrders = GetOrdersTable("Ordini Servizi", ServiceOrders);
                    string Title = @"Report "+ period +" " + startDateTime.ToString("dd/MM/yyyy") +" " +startDateTime.ToString("HH:mm") + " - " + endDateTime.ToString("dd/MM/yyyy") +" "+ endDateTime.ToString("HH:mm") ;
                    string Total = @"<table style='width: 90%;'><caption style='border-style:solid;'>Totale Prodotti & Services</caption>
                                <tr>
                                    <th colspan='2' style='width: 18%; text-align:left;'>Totale Prodotti & Servizi</th>
                                    <th style='width: 18%; text-align:left;'></th>
                                    <th style='width: 18%; text-align:left;'></th>
                                    <th style='width: 18%; text-align:left;'>Totale Prodotti & Servizi</th>
                                </tr>
                                <td colspan='2'>" + TotalNumOders +
                                   "</td><td>" +
                                   "</td><td>" +
                                   "</td><td>" + String.Format("{0,10:N}", GlobalAmount) + "€" +
                                   "</ tr ></ table >";

                    return SetMailObject(sender, Title, otherOrders, serviceOrders, Total);
                }
                return MakeEmptyMessage(sender, period);
            }
            catch (Exception e) { }
            return null;
        }

        private string GetOrdersTable(string title, List<OrderPiece> orders)
        {
            
            decimal? totalTotalAmount = 0;
            List<decimal?> gains = new List<decimal?>();

            string begin = @"
                        <table style='width: 90%; text-align:left;'>
                            <caption style='border-style:solid;'>" + title + @"</caption>
                            <thead>
                                <tr>
                                    <th style='width: 18%; text-align:left;'>Agente</th>
                                    <th style='width: 18%; text-align:left;'>Numero Ordini</th>
                                    <th style='width: 18%; text-align:left;'></th>
                                    <th style='width: 18%; text-align:left;'>Totale</th>
                                    <th style='width: 18%; text-align:left;'></th>
                                </tr>
                            </thead>
                            <tbody>";
            string middle = "";
            
            //Dictionary<string, int> countAgentOrdersDictionary = new Dictionary<string, int>();
            
            foreach (var i in orders)
            {
                /*
                middle += "<tr><td>" + i.CreationDate.Value.ToString("dd/MM/yyyy") +
                                "</td><td>" + i.SAP_ORD_ID +
                                "</td><td>" + i.CustomerDescription +
                                "</td><td>" + String.Format("{0,10:N}", i.DetailTotalAmount) +
                                " €</td><td>" + String.Format("{0,10:N}", i.Gain) +
                                 "</td><td>" + i.AgentName +
                           "</td></tr>";
                           */
                totalTotalAmount += i.DetailTotalAmount;
                gains.Add(i.Gain);
                
            }
            var groups = orders.GroupBy(o => o.AgentName);
            foreach (var agentGroup in groups)
            {
                var gainsSum = agentGroup.Sum(a => a.DetailTotalAmount); //gains
                var totaleOrdini = agentGroup.Count();
                var marginalitaAgent = agentGroup.Average(x => x.Gain);
                decimal margAgente = ( marginalitaAgent.HasValue ? marginalitaAgent.Value : 0 ) ;
                decimal.Round(margAgente, 2, MidpointRounding.AwayFromZero);
                middle += "<tr><td> "  + agentGroup.FirstOrDefault().AgentName +
                          "</td><td> "  + totaleOrdini +
                          "</td><td>"  + 
                          "</td><td>"  + String.Format("{0,10:N}", gainsSum) +//String.Format("{0;10:N}", marginalitaAgent) +
                          " €</td><td>"  + //String.Format("{0,10:N}",margAgente) +
                          "</td><td>"  +
                          "</td></tr>";
            }

            string end = @"     
                          <tfoot>
                              <tr>
                                  <th></th>
                                  <th style='text-align:left;'>" + orders.Count + @"</th>
                                  <th></th>
                                  <th style='text-align:left;'>" + String.Format("{0,10:N}", totalTotalAmount) + @" €</th>
                                  <th style='text-align:left;'>" +   @"</th>" //String.Format("{0,10:N}", gains.Average())
                              +@"</tr>
                           </tfoot>
                  </table>";
            //Add Table with Total Order & Services
            GlobalAmount += totalTotalAmount;
            GlobalGains.Add(gains.Average());
            TotalNumOders += orders.Count;

            return begin + middle + end;
        }
        
        private string GetOrdersTableProdotto(string title, List<OrderPiece> orders)
        {
            decimal? totalTotalAmount = 0;
            decimal? totalDetailAmount = 0;
            List<decimal?> gains = new List<decimal?>();

            string begin = @"
                        <table style='width: 90%; text-align:left;'>
                            <caption style='border-style:solid;'>" + title + @"</caption>
                            <thead>
                                <tr>
                                    <th style='width: 18%; text-align:left;'>Agente</th>
                                    <th style='width: 18%; text-align:left;'>Numero Ordini</th>
                                    <th style='width: 18%; text-align:left;'>Totale Prodotti Venduti</th>
                                    <th style='width: 18%; text-align:left;'>Totale Prodotti Costo</th>
                                    <th style='width: 18%; text-align:left;'>Netto Tra Veduto e Costo</th>
                                </tr>
                            </thead>
                            <tbody>";
            string middle = "";
            
            //Dictionary<string, int> countAgentOrdersDictionary = new Dictionary<string, int>();
            
            foreach (var i in orders)
            {
                /*
                middle += "<tr><td>" + i.CreationDate.Value.ToString("dd/MM/yyyy") +
                                "</td><td>" + i.SAP_ORD_ID +
                                "</td><td>" + i.CustomerDescription +
                                "</td><td>" + String.Format("{0,10:N}", i.TotalUnitCost) +
                                " €</td><td>" + String.Format("{0,10:N}", i.Gain) +
                                 "</td><td>" + i.AgentName +
                           "</td></tr>";
                           */
                totalDetailAmount += i.DetailTotalAmount;
                totalTotalAmount += i.TotalUnitCost;
                gains.Add(i.Gain);
                
            }
            var groups = orders.GroupBy(o => o.AgentName);
            foreach (var agentGroup in groups)
            {
                var gainsSum = agentGroup.Sum(a => a.DetailTotalAmount); //gains
                var gainsSumNetto = agentGroup.Sum(a => a.TotalUnitCost); //gainsNetto
                var totaleOrdini = agentGroup.Count();
                var marginalitàPerAgente = agentGroup.Average(x => x.Gain);
                decimal marginalitàPerAgenteDecimal = (marginalitàPerAgente.HasValue ? marginalitàPerAgente.Value : 0);
                
                decimal costoNettoDecimal = (gainsSumNetto.HasValue ? gainsSumNetto.Value : 0);
                var  differenzza1 = gainsSum - gainsSumNetto ;
                var marginalitàCalcolata = 100 - (100 / gainsSum * gainsSumNetto);
                decimal  differenzza = ( differenzza1.HasValue ? differenzza1.Value : 0 ) ;
                

                decimal.Round(differenzza, 2, MidpointRounding.AwayFromZero);

                middle += "<tr><td> " + agentGroup.FirstOrDefault().AgentName +
                          "</td><td> " + totaleOrdini +
                          "</td><td>" + String.Format("{0,10:N}", gainsSum) +
                          " €</td><td>" + String.Format("{0,10:N}", costoNettoDecimal) +// Totale Costo
                          " €</td><td>"+ String.Format("{0,10:N}", differenzza) + " € ("+ String.Format("{0,10:N}", marginalitàCalcolata) +" % )"+// Netto tra venduto e costo
                          "</td><td>"  +
                          "</td></tr>";
            }
            
            string end = @"     
                          <tfoot>
                              <tr>
                                  <th></th>
                                  <th style='text-align:left;'>" + orders.Count + @" </th>
                                  <th style='text-align:left;'>" + String.Format("{0,10:N}", totalDetailAmount) +@" € </th>
                                  <th style='text-align:left;'>" + String.Format("{0,10:N}", totalTotalAmount) +@" € </th>
                                  <th style='text-align:left;'>" + String.Format("{0,10:N}", (totalDetailAmount-totalTotalAmount)) + "€ ("+ String.Format("{0,10:N}", gains.Average()) +" % )"+ @" </th>
                              </tr>
                           </tfoot>
                  </table>";
            //Add Table with Total Order & Services
            GlobalAmount += totalDetailAmount;
            GlobalGains.Add(gains.Average());
            TotalNumOders += orders.Count;

            return begin + middle + end;
        }

        private MailMessage MakeEmptyMessage(string sender, string period)
        {
            MailMessage msg = new MailMessage()
            {
                From = new MailAddress(sender),
                Subject = period + " report order",
                Body = "This " + period + " has no reports",
                IsBodyHtml = true
            };
            return msg;
        }
    }
}