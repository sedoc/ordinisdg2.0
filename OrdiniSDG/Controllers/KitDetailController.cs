﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OrdiniSDG.Classes.EF;
using Newtonsoft.Json;
using Microsoft.AspNet.Identity;

namespace OrdiniSDG.Controllers
{
    public class KitDetailController : BaseController
    {
        // GET: KitDetail
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }
        [Authorize]
        public ContentResult Read(int filter = 0)
        {

            return Content(JsonConvert.SerializeObject(vw_KitDetail.getKitDetailList(filter),
                Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore }),
                "application/json");
        }

        //public JsonResult Read(int skip, int take)
        //{
        //    return Json(ORD_OrderHead.getPartialList(skip, take), JsonRequestBehavior.AllowGet);
        //}

        public ContentResult Update(string model)
        {
            return Content(JsonConvert.SerializeObject(KID_KitDetail.update(JsonConvert.DeserializeObject<KID_KitDetail>(model), int.Parse(User.Identity.GetUserId())),
                Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore }),
                "application/json");
        }
        public ContentResult Destroy(string model)
        {
            return Content(JsonConvert.SerializeObject(KID_KitDetail.delete(JsonConvert.DeserializeObject<KID_KitDetail>(model), int.Parse(User.Identity.GetUserId())),
                Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore }),
                "application/json");

        }
        public ContentResult Create(string model)
        {
            // Se il PayMethCode e il PayCondCode non ci arrivano dal modello prendo quelli di default del cliente
            return Content(JsonConvert.SerializeObject(KID_KitDetail.create(JsonConvert.DeserializeObject<KID_KitDetail>(model), int.Parse(User.Identity.GetUserId())),
                Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore }),
                "application/json");

        }
    }
}