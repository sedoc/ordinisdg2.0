﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OrdiniSDG.Classes.EF;
using Newtonsoft.Json;
using Microsoft.AspNet.Identity;
using OrdiniSDG.Classes;

namespace OrdiniSDG.Controllers
{
    public class OrderHeadController : BaseController
    {
        // GET: Order
        [Authorize]
        public ActionResult Index()
        {
            //inserisco l'id dell'utente collegato per far in modo che nella view poss riprenderlo
            ViewBag.UserId = User.Identity.GetUserId();
            return View();
        }

        public ActionResult GetState(int state)
        {
            return PartialView($"_State{state}");
        }

        [Authorize]
        public ContentResult ReadAll(string state="0",string data="",string cusordid="", string sapFilter="")
        {

            /*return Content(JsonConvert.SerializeObject(ORD_OrderHead.getOrderHead(int.Parse(state), int.Parse(User.Identity.GetUserId()), data, cusordid, sapFilter),
                Formatting.None, new JsonSerializerSettings(){ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore}),
                "application/json");*/

            //// Tutte le operazioni in Lettura verrano gestite dalla Vista vw_Order Head sul DB
            return Content(JsonConvert.SerializeObject(vw_OrderHead.getOrderHead(int.Parse(state), int.Parse(User.Identity.GetUserId()), data, cusordid, sapFilter),
                Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore }),
                "application/json");
        }

        //public JsonResult Read(int skip, int take)
        //{
        //    return Json(ORD_OrderHead.getPartialList(skip, take), JsonRequestBehavior.AllowGet);
        //}

        public ContentResult Update(string model)
        {
            return Content(JsonConvert.SerializeObject(ORD_OrderHead.update(JsonConvert.DeserializeObject<ORD_OrderHead>(model), int.Parse(User.Identity.GetUserId())),
                Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore }),
                "application/json");
        }
        public ContentResult Destroy(string model)
        {
            return Content(JsonConvert.SerializeObject(ORD_OrderHead.delete(JsonConvert.DeserializeObject<ORD_OrderHead>(model), int.Parse(User.Identity.GetUserId())),
                Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore }),
                "application/json");
        }

        public ContentResult Create(string model)
        {
            // Se il PayMethCode e il PayCondCode non ci arrivano dal modello prendo quelli di default del cliente
            return Content(JsonConvert.SerializeObject(ORD_OrderHead.create(JsonConvert.DeserializeObject<ORD_OrderHead>(model), int.Parse(User.Identity.GetUserId())),
                Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore }),
                "application/json");
        }

        [HttpPost]
        public JsonResult OrderConfirm(string orderHeadId)
        {
            // Se il PayMethCode e il PayCondCode non ci arrivano dal modello prendo quelli di default del cliente
            //return Content(JsonConvert.SerializeObject(ORD_OrderHead.create(JsonConvert.DeserializeObject<ORD_OrderHead>(model), int.Parse(User.Identity.GetUserId())),
            //    Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore }),
            //    "application/json");
            return Json(ORD_OrderHead.orderConfirm(int.Parse(orderHeadId), int.Parse(User.Identity.GetUserId())));
        }
        public JsonResult OrderApprove(string orderHeadId)
        {
            return Json(ORD_OrderHead.OrderApprove(int.Parse(orderHeadId)));
        }
    }
}