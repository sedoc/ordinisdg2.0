﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using OrdiniSDG.Classes;
using OrdiniSDG.Classes.EF;

namespace OrdiniSDG.Controllers
{
    public class BaseController : Controller
    {
        public static long bytesToMegaBytes(long bytes)
        {
            return (bytes / 1024) / 1024;
        }
        

        /* 
         * Metodo per recuperare l'utente dalla sessione
         */
        //public int getUserSession()
        //{
        //    try
        //    {
        //        return (int)HttpContext.Session[Globals.sessionUserIdKey];
        //    }
        //    catch
        //    {
        //        return 0;
        //    }
        //}
    }
}