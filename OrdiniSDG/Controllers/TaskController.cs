﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Reflection;
using System.Web.Mvc;
using OrdiniSDG.Classes.EF;
using Newtonsoft.Json;

namespace OrdiniSDG.Controllers
{
    public class TaskController : BaseController
    {
        // GET: Task
        public String CallWsSap()
        {
            SAPService1.Service1SoapClient ws_Sap1Client = new SAPService1.Service1SoapClient();
            //Chiamo aggiornamento di SAP, commentato perchè troppo lento , probabimente lo schedulerò esternamento via task sched
            return  "Result : "+ws_Sap1Client.Importazione_Ordini();
        }

        public String UpdateOrderDataFromSAP()
        {
            
            return "Result : " + ORD_OrderHead.orderUpdateSapId();
        }
    }
}