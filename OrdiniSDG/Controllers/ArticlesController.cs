﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OrdiniSDG.Classes.EF;
using Newtonsoft.Json;
using Microsoft.AspNet.Identity;

namespace OrdiniSDG.Controllers
{
    public class ArticlesController : BaseController
    {
        // GET: Articles
        public ActionResult Index()
        {
            return View();
        }
        [Authorize]
        public JsonResult ReadArtCGross(string filter = null)
        {
            return Json(ListinoComputerGross.searchItem(filter), JsonRequestBehavior.AllowGet);
        }
        [Authorize]
        public JsonResult ReadArtSap(string filter = null)
        {
            return Json(SAP_Articoli.searchItem(filter), JsonRequestBehavior.AllowGet);
        }

        //Leggo classi articoli da SAP
        
        public JsonResult ReadAreas(string filter = null)
        {
            return Json(SAP_AREA_AFFARI.getList(filter), JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult ReadClasses(string filter = null)
        {
            return Json(SAP_CLASSEV.getList(filter), JsonRequestBehavior.AllowGet);
        }
        public JsonResult ReadSectors(string filter = null)
        {
            return Json(SAP_SETTORE.getList(filter), JsonRequestBehavior.AllowGet);
        }

    }
}