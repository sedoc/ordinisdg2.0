﻿using OrdiniSDG.Models;
using OrdiniSDG.Repositories;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using OrdiniSDG.Classes.EF;
using OrdiniSDG.Classes;

namespace OrdiniSDG.Controllers
{
    public class AccountController : BaseController
    {
        private readonly IAuthenticationManager _auth;
        private readonly IAccount _accountServices;

        public AccountController(IAuthenticationManager auth, IAccount accountServices)
        {
            this._auth = auth;
            this._accountServices = accountServices;
        }

        // GET: Account
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");

            ViewBag.ReturnUrl = returnUrl;
            return View(new LoginViewModel());
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
                return View();
            Classes.EF.USR_User userDb = this._accountServices.getUserFromUsername(model.UserName);

            if (userDb != null && PasswordHash.ValidatePassword(model.Password, userDb.Password))
            {
                var identity = new ClaimsIdentity(new[] {
                    new Claim(ClaimTypes.Name, model.UserName),
                    new Claim(ClaimTypes.Role,userDb.Role.ToString()),
                    new Claim(ClaimTypes.Email,userDb.Email),
                    new Claim(ClaimTypes.NameIdentifier,userDb.USR_ID.ToString()),
                    }, DefaultAuthenticationTypes.ApplicationCookie);

                this._auth.SignIn(new AuthenticationProperties
                {
                    IsPersistent = model.RememberMe
                }, identity);
                
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ModelState.AddModelError("", "Invalid login attempt.");
                return View(model);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            this._auth.SignOut();
            return RedirectToAction("Login", "Account");
        }

        [Authorize(Roles = "1")]
        public ActionResult CreateUser()
        {
            return View();
        }

        [HttpGet]
        public ContentResult GetUsers()
        {
            return Content(JsonConvert.SerializeObject(this._accountServices.getUsers()),
                "application/json");
        }

        public ContentResult AddUser(string model)
        {
            return Content(JsonConvert.SerializeObject(this._accountServices.addUser(JsonConvert.DeserializeObject<USR_User>(model))),
                "application/json");
        }

        public ContentResult UpdateUser(string model)
        {
            return Content(JsonConvert.SerializeObject(this._accountServices.updateUser(JsonConvert.DeserializeObject<USR_User>(model))),
                "application/json");
        }

        [HttpGet]
        public ContentResult GetRoles()
        {
            return Content(JsonConvert.SerializeObject(this._accountServices.getRoles()),
                "application/json");
        }

        [HttpGet]
        public ContentResult IsExistUserName(string userName)
        {
            return Content(JsonConvert.SerializeObject(this._accountServices.isExistUserName(userName)),
                "application/json");
        }
    }
}