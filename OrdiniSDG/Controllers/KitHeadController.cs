﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OrdiniSDG.Classes.EF;
using Newtonsoft.Json;
using Microsoft.AspNet.Identity;

namespace OrdiniSDG.Controllers
{
    public class KitHeadController : BaseController
    {
        // GET: KitHead
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ContentResult ReadAll()
        {
            //Leggo dalla vista per avere il prezzo totale sempre aggiornato
            return Content(JsonConvert.SerializeObject(vw_KitHead.getKitHead( int.Parse(User.Identity.GetUserId())),
                Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore }),
                "application/json");
        }

        //public JsonResult Read(int skip, int take)
        //{
        //    return Json(KIH_KitHead.getPartialList(skip, take), JsonRequestBehavior.AllowGet);
        //}

        public ContentResult Update(string model)
        {
            return Content(JsonConvert.SerializeObject(KIH_KitHead.update(JsonConvert.DeserializeObject<KIH_KitHead>(model), int.Parse(User.Identity.GetUserId())),
                Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore }),
                "application/json");
        }
        public ContentResult Destroy(string model)
        {
            return Content(JsonConvert.SerializeObject(KIH_KitHead.delete(JsonConvert.DeserializeObject<KIH_KitHead>(model), int.Parse(User.Identity.GetUserId())),
                Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore }),
                "application/json");

        }
        public ContentResult Create(string model)
        {
            return Content(JsonConvert.SerializeObject(KIH_KitHead.create(JsonConvert.DeserializeObject<KIH_KitHead>(model), int.Parse(User.Identity.GetUserId())),
                Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore }),
                "application/json");

        }
    }
}