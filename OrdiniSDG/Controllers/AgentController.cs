﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Reflection;
using System.Web.Mvc;
using OrdiniSDG.Classes.EF;
using Newtonsoft.Json;
using Microsoft.AspNet.Identity;

namespace OrdiniSDG.Controllers
{
    public class AgentController : BaseController
    {
        // GET: Agent
        public ActionResult Index()
        {
            return View();
        }
        [Authorize]
        public JsonResult Read(string filter = null)
        {
            return Json(SAP_Agenti.getList(filter), JsonRequestBehavior.AllowGet);
        }
    }
}