﻿using Mvc.Mailer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrdiniSDG.Mailers
{
    public class UserMailers : MailerBase
    {
        public UserMailers()
		{
			MasterName="_Layout";
		}

        public virtual MvcMailMessage passwordLost(string userEmail, string actionUrl)
        {
            ViewBag.Email = userEmail;
            ViewBag.url = actionUrl;

            return Populate(x =>
            {
                x.Subject = "Ordini SDG|Cambio Password";
                x.ViewName = "PasswordLost";
                x.To.Add(userEmail);
            });
        }
    }
}